# Szewmic
## Code challenge

Solution to Restaurant Club code challenge. Application rendering data fetched from server. Data describes projects with randomly placed rectangles.

## Features

- Loading random projects
- Loading project by given ID
- Canvas displaying project items (item, center point, bounding box)

## Tech

Used in solution technologies:

- [React] 
- [SVG] - for rendering graphics
- [TypeScript] - for safe typing
- [Redux] - global app state management
- [ReduxToolkit] - to avoid writing boilerplatte,
- [Redux Toolkit Query] - to invalidate server data cache and avoid uneccesary fetching data
- [Styled-Components] - to write encapsulated css in js styles, just personal preference
- [CRA] - create react app setup for speeding up development

## Installation

Install the dependencies and devDependencies and start the server.

```sh
npm install
npm run start
```



## License

MIT
## License

MIT