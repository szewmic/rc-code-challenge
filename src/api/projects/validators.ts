import { ProjectItem } from "../../models/ProjectItem";

const validateProjectItem = (projectItem: ProjectItem): boolean => {
  const { width, height, x, y, rotation } = projectItem;
  const numbers = [width, height, x, y, rotation];
  if (rotation < 0 && rotation > 360) {
    return false;
  }

  if (width <= 0 || height <= 0 || x <= 0 || y <= 0) {
    return false;
  }

  if (numbers.some((it) => isNaN(it))) {
    return false;
  }

  return true;
};

const validateProjectItems = (projectItems: ProjectItem[]): boolean => {
  return projectItems.some((pi) => !validateProjectItem(pi));
};

export { validateProjectItem, validateProjectItems };
