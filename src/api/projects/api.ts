import { createApi, fetchBaseQuery } from "@reduxjs/toolkit/query/react";
import { config } from "../../hostinfo";
import { Project } from "../../models/Project";
import { GetProjectResponse } from "./types";
import { validateProjectItems } from "./validators";

export const PROJECTS_API_REDUCER_KEY = "projectsApi";

export const projectsApi = createApi({
  reducerPath: PROJECTS_API_REDUCER_KEY,
  baseQuery: fetchBaseQuery({ baseUrl: config.api.baseUrl }),
  endpoints: (builder) => ({
    getProjectById: builder.query<Project, string>({
      queryFn: async (id, api, extraOptions, fetchWithBQ) => {
        const result = await fetchWithBQ(`/project/${id}`);
        const response: GetProjectResponse = result.data as GetProjectResponse;
        const project = response?.project;
        if (!project.items || validateProjectItems(project.items)) {
          return {
            error: { status: 500, statusText: "Invalid data", data: [] }
          };
        }
        return {
          data: project
        };
      }
    })
  }),
  refetchOnMountOrArgChange: 60
});

export const { useGetProjectByIdQuery } = projectsApi;
