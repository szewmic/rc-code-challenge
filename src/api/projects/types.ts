import { Project } from "../../models/Project";

export interface GetProjectResponse {
  readonly id: string;
  readonly project: Project;
}
