import { createSlice, PayloadAction } from "@reduxjs/toolkit";
import { ProjectCanvasState } from "./types";

const initialState: ProjectCanvasState = {
  selectedProjectId: ""
};

export const projectCanvasSlice = createSlice({
  name: "vehicleSelectorSlice",
  initialState,
  reducers: {
    setSelectedProject(state, action: PayloadAction<string>) {
      state.selectedProjectId = action.payload;
    }
  }
});

export const { setSelectedProject } = projectCanvasSlice.actions;
const { reducer } = projectCanvasSlice;
export default reducer;
