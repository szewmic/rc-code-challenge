import React from "react";
import { Canvas, Rectangle, Retry } from "../../common/components";
import { useGetProjectByIdQuery } from "../../api/projects/api";
import { ProjectHeader } from "./ProjectHeader/index";
import { ProjectCanvasContainer } from "./styles";

export interface ProjectCanvasProps {
  readonly projectId: string;
}

const ProjectCanvas = ({ projectId }: ProjectCanvasProps) => {
  const { data: project, isError, error, refetch } = useGetProjectByIdQuery(
    projectId
  );

  if (isError && error) {
    return (
      <Retry
        onRetry={refetch}
        message={"Oops cannot load project"}
        type={"dark"}
      ></Retry>
    );
  }

  return (
    <ProjectCanvasContainer>
      {project && <ProjectHeader project={project}></ProjectHeader>}
      {project && (
        <svg
          width="100%"
          height="100%"
          fill="#d3d3d3"
          preserveAspectRatio="xMidYMid meet"
        >
          <Canvas width={project?.width} height={project?.height}>
            {project &&
              project &&
              project?.items.map((rect, i) => {
                return <Rectangle key={i} {...rect} />;
              })}
          </Canvas>
        </svg>
      )}
    </ProjectCanvasContainer>
  );
};

export default ProjectCanvas;
