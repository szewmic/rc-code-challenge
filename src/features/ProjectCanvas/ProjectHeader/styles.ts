import Styled from "styled-components";

export const ProjectHeaderContainer = Styled.div`
background: white;
padding: 1px;
`;

export const ProjectHeaderNameRow = Styled.p`
margin-left: 3px;
`;

export const ProjectHeaderNameLabel = Styled.span`
font-weight: bold;
margin-left: 3px;
`;

export const ProjectHeaderIdRow = Styled.p`
margin-top: 3px;
margin-left: 3px;
`;

export const ProjectHeaderSelectorInput = Styled.input`
margin-left: 3px;
width: 55vh;
`;
