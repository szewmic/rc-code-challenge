import React from "react";
import { Project } from "../../../models/Project";
import {
  ProjectHeaderContainer,
  ProjectHeaderNameLabel,
  ProjectHeaderIdRow,
  ProjectHeaderNameRow,
  ProjectHeaderSelectorInput
} from "./styles";

export interface ProjectHeaderProps {
  readonly project: Project;
}

export const ProjectHeader = ({ project }: ProjectHeaderProps) => {
  return (
    <ProjectHeaderContainer>
      <ProjectHeaderNameRow>
        Name: <ProjectHeaderNameLabel>{project.name}</ProjectHeaderNameLabel>
      </ProjectHeaderNameRow>
      <ProjectHeaderIdRow>
        ID:
        <ProjectHeaderSelectorInput value={project.id} disabled />
      </ProjectHeaderIdRow>
    </ProjectHeaderContainer>
  );
};
