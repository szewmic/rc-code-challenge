import { useCallback, useState } from "react";
import { useTypedDispatch } from "../../common/store";
import { setSelectedProject } from "../ProjectCanvas/slice";
import { config } from "../../hostinfo";
import {
  ProjectChooserContainer,
  ProjectChooserInput,
  ProjectChooserButton
} from "./styles";

export default () => {
  const [inputVal, setInputVal] = useState<string>();
  const dispatch = useTypedDispatch();

  const onChange = useCallback((e) => {
    setInputVal(e.target.value);
  }, []);

  const onClick = useCallback(
    async (e) => {
      if (inputVal) {
        dispatch(setSelectedProject(inputVal));
      } else {
        const res: any = await fetch(config.api.baseUrl + "init");
        const resJson: any = await res.json();

        if (resJson.id) {
          dispatch(setSelectedProject(resJson.id));
        }
      }
    },
    [dispatch, inputVal]
  );

  return (
    <ProjectChooserContainer>
      <ProjectChooserInput
        placeholder={"Leave empty for random project"}
        value={inputVal}
        onChange={onChange}
      ></ProjectChooserInput>
      <ProjectChooserButton onClick={onClick}>
        Load Project
      </ProjectChooserButton>
    </ProjectChooserContainer>
  );
};
