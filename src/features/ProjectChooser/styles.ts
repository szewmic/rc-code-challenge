import Styled from "styled-components";

export const ProjectChooserContainer = Styled.div`
width: 100%;
height: 100%;
`;

export const ProjectChooserInput = Styled.input`
min-width: 400px;
margin-right: 5px;
`;

export const ProjectChooserButton = Styled.button`
width: 90px;
`;
