import React from "react";

export interface CircleProps {
  readonly x: number;
  readonly y: number;
  readonly radius?: number;
  readonly color?: string;
}

const Circle = ({ x, y, radius, color }: CircleProps) => {
  return <circle fill={color} cx={x} cy={y} r={radius}></circle>;
};

Circle.defaultProps = {
  radius: 4,
  color: "#FFFFFF"
};

export default Circle;
