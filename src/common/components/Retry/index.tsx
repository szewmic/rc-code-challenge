import { RetryButton, RetryMessage } from "./styles";

interface RetryProps {
  readonly type?: "base" | "dark";
  readonly message?: string;
  readonly onRetry: () => void;
}

const Retry = ({ type, message, onRetry }: RetryProps) => {
  return (
    <div>
      <RetryMessage type={type ?? "base"}>
        {message ?? "Oops cannot load data"}
      </RetryMessage>
      <RetryButton onClick={(e) => onRetry()}>Retry</RetryButton>
    </div>
  );
};

export default Retry;
