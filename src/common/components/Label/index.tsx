import React from "react";

export interface LabelProps {
  readonly x: number;
  readonly y: number;
  readonly text: string;
  readonly color?: string;
}

const Label = ({ x, y, text, color }: LabelProps) => {
  return (
    <text x={x} y={y} fill={color}>
      <tspan>{text}</tspan>
    </text>
  );
};

Label.defaultProps = {
  color: "#FFFFFF"
};

export default Label;
