import React from "react";
import { Circle, Label } from "../index";
import { BoundingBox } from "./components/BoundingBox/index";
import { BaseRectangle } from "./components/BaseRectangle/index";
import { pickTextColorBasedOnBgColor } from "./utils";

const ROTATION_LABEL_OFFSET = 5;
const CIRCLE_POINT_RADIUS = 4;
const DEFAULT_ROTATION = 0;
const DEFAULT_COLOR = "#000000";

export interface RectangleProps {
  readonly x: number;
  readonly y: number;
  readonly width: number;
  readonly height: number;
  readonly rotation?: number;
  readonly color?: string;
  readonly showBBox: boolean;
  readonly showRotationLabel?: boolean;
  readonly rotationLabelOffset?: number;
  readonly rotationLabelColor?: string;
  readonly showCenterPoint?: boolean;
  readonly centerPointRadius?: number;
  readonly centerPointColor?: string;
}

const Rectangle = ({
  x,
  y,
  width,
  height,
  rotation,
  color,
  showBBox,
  showRotationLabel,
  rotationLabelOffset,
  rotationLabelColor,
  showCenterPoint,
  centerPointRadius,
  centerPointColor
}: RectangleProps) => {
  return (
    <g>
      <BaseRectangle
        width={width}
        height={height}
        color={color}
        x={x}
        y={y}
        rotation={rotation}
      />
      {showCenterPoint && (
        <Circle
          x={x}
          y={y}
          radius={centerPointRadius ?? CIRCLE_POINT_RADIUS}
          color={
            centerPointColor ??
            pickTextColorBasedOnBgColor(color ?? DEFAULT_COLOR)
          }
        />
      )}
      {showRotationLabel && rotation && (
        <Label
          text={rotation + "°"}
          color={
            rotationLabelColor ??
            pickTextColorBasedOnBgColor(color ?? DEFAULT_COLOR)
          }
          x={x + (rotationLabelOffset ?? ROTATION_LABEL_OFFSET)}
          y={y}
        />
      )}
      {showBBox && (
        <BoundingBox
          width={width}
          height={height}
          x={x}
          y={y}
          rotation={rotation}
        ></BoundingBox>
      )}
    </g>
  );
};

Rectangle.defaultProps = {
  rotation: DEFAULT_ROTATION,
  showRotationLabel: true,
  showCenterPoint: true,
  showBBox: true
};

export default Rectangle;
