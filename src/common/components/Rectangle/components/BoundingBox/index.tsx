import React from "react";
import { useBBox } from "./hooks/useBBox";
import { BaseRectangle } from "../BaseRectangle/index";

interface BoundingBoxProps {
  readonly width: number;
  readonly height: number;
  readonly x: number;
  readonly y: number;
  readonly rotation?: number;
  readonly strokeWidth?: number;
  readonly stroke?: string;
  readonly strokeOpacity?: string;
}

const BoundingBox = ({
  width,
  height,
  x,
  y,
  rotation,
  stroke,
  strokeWidth,
  strokeOpacity
}: BoundingBoxProps) => {
  const { bbWidth, bbHeight } = useBBox({ width, height, rotation });
  return (
    <BaseRectangle
      x={x}
      y={y}
      width={bbWidth}
      height={bbHeight}
      color="none"
      stroke={stroke ?? "#FF0000"}
      strokeWidth={strokeWidth ?? 2}
      strokeOpacity={strokeOpacity ?? "0.4"}
    />
  );
};

BoundingBox.defaultProps = {
  x: 0,
  y: 0
};

export { BoundingBox };
