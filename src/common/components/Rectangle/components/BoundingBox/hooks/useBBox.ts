import { useMemo } from "react";

export interface UseBBProps {
  readonly width: number;
  readonly height: number;
  readonly rotation?: number;
}

export const useBBox = ({ width, height, rotation }: UseBBProps) => {
  const bbHeight = useMemo(() => {
    if (!rotation) return height;
    return (
      width * Math.abs(Math.sin((rotation * Math.PI) / 180)) +
      height * Math.abs(Math.cos((rotation * Math.PI) / 180))
    );
  }, [width, height, rotation]);

  const bbWidth = useMemo(() => {
    if (!rotation) return width;
    return (
      width * Math.abs(Math.cos((rotation * Math.PI) / 180)) +
      height * Math.abs(Math.sin((rotation * Math.PI) / 180))
    );
  }, [width, height, rotation]);

  return {
    bbHeight,
    bbWidth
  };
};
