import React from "react";

interface BaseRectangleProps {
  readonly width: number;
  readonly height: number;
  readonly x?: number;
  readonly y?: number;
  readonly rotation?: number;
  readonly color?: string;
  readonly strokeWidth?: number;
  readonly stroke?: string;
  readonly strokeOpacity?: string;
}

const BaseRectangle = ({
  color,
  width,
  height,
  x,
  y,
  rotation,
  stroke,
  strokeWidth,
  strokeOpacity
}: BaseRectangleProps) => {
  const transformers = [];
  if (x && y) {
    transformers.push(`translate(${x}, ${y})`);
    if (rotation) {
      transformers.push(`rotate(${rotation})`);
    }
    transformers.push(`translate(-${width / 2},-${height / 2})`);
  }
  return (
    <rect
      width={width}
      height={height}
      fill={color}
      transform={transformers.join(" ")}
      strokeOpacity={strokeOpacity}
      stroke={stroke}
      strokeWidth={strokeWidth}
    />
  );
};

BaseRectangle.defaultProps = {
  color: "black",
  x: 0,
  y: 0,
  strokeWidth: 3,
  stroke: "rgb(0,0,0)"
};

export { BaseRectangle };
