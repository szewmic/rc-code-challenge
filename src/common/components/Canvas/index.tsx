import React, { ReactNode } from "react";

export interface CanvasProps {
  readonly width: number;
  readonly height: number;
  readonly bgColor?: string;
  readonly children?: ReactNode[] | ReactNode;
}

const Canvas = ({ width, height, bgColor, children }: CanvasProps) => {
  return (
    <svg
      version="1.1"
      xmlns="http://www.w3.org/2000/svg"
      width="100%"
      height="100%"
      viewBox={`0 0 ${width} ${height}`}
    >
      <rect fill={bgColor} width="100%" height="100%"></rect>
      {children}
    </svg>
  );
};

Canvas.defaultProps = {
  bgColor: "#efefef"
};

export default Canvas;
