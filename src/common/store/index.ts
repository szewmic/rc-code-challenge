import { combineReducers, configureStore } from "@reduxjs/toolkit";
import { PROJECTS_API_REDUCER_KEY, projectsApi } from "../../api/projects/api";
import { TypedUseSelectorHook, useDispatch, useSelector } from "react-redux";
import ProjectCanvasReducer, {
  projectCanvasSlice
} from "../../features/ProjectCanvas/slice";

const reducers = {
  [PROJECTS_API_REDUCER_KEY]: projectsApi.reducer,
  [projectCanvasSlice.name]: ProjectCanvasReducer
};

const combinedReducer = combineReducers<typeof reducers>(reducers);

export const store = configureStore({
  reducer: combinedReducer,
  middleware: (getDefaultMiddleware) =>
    getDefaultMiddleware().concat([projectsApi.middleware]),
  devTools: true
});

export type RootState = ReturnType<typeof combinedReducer>;
export type AppDispatch = typeof store.dispatch;

export const useTypedDispatch = () => useDispatch<AppDispatch>();
export const useTypedSelector: TypedUseSelectorHook<RootState> = useSelector;
