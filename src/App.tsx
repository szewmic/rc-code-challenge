import "./styles.css";
import ProjectCanvas from "./features/ProjectCanvas/index";
import ProjectChooser from "./features/ProjectChooser/index";
import { RootState, useTypedSelector } from "./common/store";

export default function App() {
  const selectedProjectId = useTypedSelector(
    (state: RootState) => state.vehicleSelectorSlice.selectedProjectId
  );

  return (
    <div className="App">
      <main>
        <div>
          <ProjectChooser />
        </div>
        {selectedProjectId && <ProjectCanvas projectId={selectedProjectId} />}
      </main>
    </div>
  );
}
