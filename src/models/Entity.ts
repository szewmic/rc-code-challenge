export interface Entity<T> {
  readonly id: T;
}
