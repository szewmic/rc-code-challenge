import { Entity } from "./Entity";
import { ProjectItem } from "./ProjectItem";

export interface Project extends Entity<string> {
  readonly name: string;
  readonly width: number;
  readonly height: number;
  readonly items: ProjectItem[];
}
