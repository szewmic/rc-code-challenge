import { Entity } from "./Entity";

export interface ProjectItem extends Entity<string> {
  readonly color: string;
  readonly rotation: number;
  readonly x: number;
  readonly y: number;
  readonly width: number;
  readonly height: number;
}
